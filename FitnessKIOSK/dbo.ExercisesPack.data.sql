﻿SET IDENTITY_INSERT [dbo].[ExercisesPack] ON
INSERT INTO [dbo].[ExercisesPack] ([Id], [Title], [Calories], [Time]) VALUES (1, N'Reps (Women)', N'294', 14)
INSERT INTO [dbo].[ExercisesPack] ([Id], [Title], [Calories], [Time]) VALUES (2, N'Tabata (Women)', N'364', 13)
INSERT INTO [dbo].[ExercisesPack] ([Id], [Title], [Calories], [Time]) VALUES (3, N'Paleo Run (Women)', N'140', 10)
INSERT INTO [dbo].[ExercisesPack] ([Id], [Title], [Calories], [Time]) VALUES (4, N'Reps (Men)', N'294', 14)
INSERT INTO [dbo].[ExercisesPack] ([Id], [Title], [Calories], [Time]) VALUES (5, N'Tabata (Men)', N'364', 13)
INSERT INTO [dbo].[ExercisesPack] ([Id], [Title], [Calories], [Time]) VALUES (6, N'Paleo Run (Men)', N'140', 10)
SET IDENTITY_INSERT [dbo].[ExercisesPack] OFF
