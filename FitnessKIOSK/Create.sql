﻿-- Creating table 'Students'
CREATE TABLE [dbo].[Users] (
	[Id] int IDENTITY(1,1) NOT NULL,
	[FirstName]  nvarchar(max) NOT NULL,
	[LastName] nvarchar(max) NOT NULL,
	[DateOfBirth] date NOT NULL,
		PRIMARY KEY (Id)
);
GO

--CREATE TABLE [dbo].[BodyMassIndexData] (
--	[Id] int IDENTITY(1,1) NOT NULL,
--	[Age] int NOT NULL,
--	[BMIMinValue] float NOT NULL,
--	[BMIMaxValue] float NOT NULL,
--	[Description] nvarchar(2000) NOT NULL,
--		PRIMARY KEY (Id),
--);
--GO

CREATE TABLE [dbo].[BodyMassIndex] (
	[Id] int IDENTITY(1,1) NOT NULL,
	[Height]  float NOT NULL,
	[Weight] float NOT NULL,
	[Gender] nvarchar(10) NOT NULL,
	[Age] int NOT NULL,
	[CalculatedOn] date NOT NULL,
	[BMIValue] float NOT NULL,
	[UserId] int NOT NULL,
		PRIMARY KEY (Id),
		FOREIGN KEY (UserId) REFERENCES Users(Id)
);
GO


