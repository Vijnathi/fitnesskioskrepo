﻿SET IDENTITY_INSERT [dbo].[Exercises] ON
INSERT INTO [dbo].[Exercises] ([Id], [Title], [Description], [ExercisesPackId]) VALUES (1, N'10x Push-ups', N'hands knee height', 1)
INSERT INTO [dbo].[Exercises] ([Id], [Title], [Description], [ExercisesPackId]) VALUES (2, N'10x Iron crosses', N'knees bent', 1)
INSERT INTO [dbo].[Exercises] ([Id], [Title], [Description], [ExercisesPackId]) VALUES (3, N'10x Mountain climbers', N'knees to opposite elbow', 1)
INSERT INTO [dbo].[Exercises] ([Id], [Title], [Description], [ExercisesPackId]) VALUES (4, N'Russian twists', N'count left-right as one', 1)
INSERT INTO [dbo].[Exercises] ([Id], [Title], [Description], [ExercisesPackId]) VALUES (5, N'00:20 Fast feet', N'8 times, after each time rest for 10 seconds', 2)
INSERT INTO [dbo].[Exercises] ([Id], [Title], [Description], [ExercisesPackId]) VALUES (6, N'00:20 Boat', N'8 times, after each time rest for 10 seconds', 2)
INSERT INTO [dbo].[Exercises] ([Id], [Title], [Description], [ExercisesPackId]) VALUES (7, N'00:20 Reverse Push-ups', N'8 times, after each time rest for 10 seconds', 2)
INSERT INTO [dbo].[Exercises] ([Id], [Title], [Description], [ExercisesPackId]) VALUES (9, N'00:20 Forward lunges', N'8 times, after each time rest for 10 seconds. After 4 times, switch leg.', 2)
INSERT INTO [dbo].[Exercises] ([Id], [Title], [Description], [ExercisesPackId]) VALUES (10, N'8x Push-ups', N'hands on floor. 1 minute cardio(run/jog) after finishing.', 3)
INSERT INTO [dbo].[Exercises] ([Id], [Title], [Description], [ExercisesPackId]) VALUES (11, N'8x Dips', N'Seated (legs straight).  1 minute cardio(run/jog) after finishing.', 3)
INSERT INTO [dbo].[Exercises] ([Id], [Title], [Description], [ExercisesPackId]) VALUES (12, N'8x Squats', N'deep. 1 minute cardio(run/jog) after finishing.', 3)
SET IDENTITY_INSERT [dbo].[Exercises] OFF
