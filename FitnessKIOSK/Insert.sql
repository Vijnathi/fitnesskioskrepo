﻿INSERT INTO [dbo].[BodyMassIndexData] (Age, Gender, BMIMinValue, BMIMaxValue, Description) VALUES (
	20, 30, 100, 'Change your diet: eat healthy deal, cut back sugar, watch out carbohydrates;Get enough rest: at least 7 hours;exercises;
							Check your BMI every week'
);

INSERT INTO [dbo].[BodyMassIndexData] (Age, Gender, BMIMinValue, BMIMaxValue, Description) VALUES (
	40, 0, 18.5, 'add calories to your diet;start training to gain lean muscle instead of just body fat;have a snack as soon as you feel hungry'
);

INSERT INTO [dbo].[BodyMassIndexData] (Age, Gender, BMIMinValue, BMIMaxValue, Description) VALUES (
	18, 18.5, 24.9, 'Get enough rest: try to sleep at least 7 hours;Stay active and do exersices;Eat mindfully: try to have only healthy meal'
);

INSERT INTO [dbo].[BodyMassIndexData] (Age, Gender, BMIMinValue, BMIMaxValue, Description) VALUES (
	18, 25, 29.9, 'Get enough rest: try to sleep at least 7 hours;Stay active and do exersices;Eat mindfully: try to have only healthy meal'
);
