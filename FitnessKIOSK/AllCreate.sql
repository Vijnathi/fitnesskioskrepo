﻿CREATE TABLE [dbo].[Users] (
	[Id] int IDENTITY(1,1) NOT NULL,
	[FirstName]  nvarchar(max) NOT NULL,
	[LastName] nvarchar(max) NOT NULL,
	[DateOfBirth] date NOT NULL,
		PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

CREATE TABLE [dbo].[BodyMassIndex] (
	[Id] int IDENTITY(1,1) NOT NULL,
	[Height]  float NOT NULL,
	[Weight] float NOT NULL,
	[Gender] nvarchar(10) NOT NULL,
	[Age] int NOT NULL,
	[CalculatedOn] date NOT NULL,
	[BMIValue] float NOT NULL,
	[UserId] int NOT NULL,
		PRIMARY KEY CLUSTERED ([Id] ASC),
		FOREIGN KEY (UserId) REFERENCES Users(Id)
);
GO

CREATE TABLE [dbo].[Events] (
	[Id] int IDENTITY(1,1) NOT NULL,
	[Title]  nvarchar(max) NOT NULL,
	[Description] nvarchar(max) NOT NULL,
	[Start] datetime NOT NULL,
	[End] datetime NOT NULL,
	[ThemeColour] nvarchar(max) NOT NULL,
	[isAllDay] bit NOT NULL,
	[UserId] int NOT NULL,
		PRIMARY KEY CLUSTERED ([Id] ASC),
		FOREIGN KEY (UserId) REFERENCES Users(Id)
);
GO

CREATE TABLE [dbo].[ExercisesPack] (
	[Id] int IDENTITY(1,1) NOT NULL,
	[Title]  nvarchar(max) NOT NULL,
	[Calories] nvarchar(max) NOT NULL,
	[Time] datetime NOT NULL
		PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

CREATE TABLE [dbo].[Exercises] (
	[Id] int IDENTITY(1,1) NOT NULL,
	[Title]  nvarchar(max) NOT NULL,
	[Description] nvarchar(max) NOT NULL,
	[ExercisesPackId] int NOT NULL,
		PRIMARY KEY CLUSTERED ([Id] ASC),
		FOREIGN KEY (ExercisesPackId) REFERENCES ExercisesPack(Id)
);
GO

CREATE TABLE [dbo].[Likes] (
	[Id] int IDENTITY(1,1) NOT NULL,
	[isLike] bit NOT NULL,
	[UserId] int NOT NULL,
	[ExercisesId] int NOT NULL,
		PRIMARY KEY CLUSTERED ([Id] ASC),
		FOREIGN KEY (UserId) REFERENCES Users(Id),
		FOREIGN KEY (ExercisesId) REFERENCES Exercises(Id)
);
GO
