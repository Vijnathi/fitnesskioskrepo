﻿using FitnessKIOSK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace FitnessKIOSK.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Home()
        {
            ViewBag.Message = "Your Home page.";

            return View();
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task Contact(EmailFormModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var body = "<p>Email From: {0} ({1})Message:</p><p>{2}</p>";
        //        var message = new MailMessage();
        //        //message.To.Add(new MailAddress("recipient@gmail.com"));  // replace with valid value 
        //        message.To.Add(new MailAddress("christopher.messom@monash.edu"));  // replace with valid value 
        //        //message.From = new MailAddress("sender@outlook.com");  // replace with valid value
        //        message.From = new MailAddress();  // replace with valid value
        //        message.Subject = "Your email subject";
        //        message.Body = string.Format(body, model.FromName, model.FromEmail, model.Message);
        //        message.IsBodyHtml = true;

        //        using (var smtp = new SmtpClient())
        //        {
        //            var credential = new NetworkCredential
        //            {
        //                UserName = "user@outlook.com",  // replace with valid value                                
        //                // Password = "password"        // replace with valid value
        //            };
        //            smtp.Credentials = credential;
        //            //smtp.Host = "smtp-mail.outlook.com";
        //            smtp.Host = "smtp.monash.edu.au";
        //            //smtp.Port = 587;
        //            //smtp.EnableSsl = true;
        //            await smtp.SendMailAsync(message);
        //            return RedirectToAction("Sent");
        //        }
        //    }
        //    return View(model);
        //}

        //public ActionResult Sent()
        //{
        //    return View();
        //}
    }
}