﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FitnessKIOSK.Models;

namespace FitnessKIOSK.Controllers
{
    public class ExercisesPacksController : Controller
    {
        private FitnessKIOSK_Model db = new FitnessKIOSK_Model();

        // GET: ExercisesPacks
        public ActionResult Index()
        {
            return View(db.ExercisesPacks.ToList());
        }

        // GET: ExercisesPacks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //ExercisesPack exercisesPack = db.ExercisesPacks.Find(id);
            Exercis exercis = db.Exercises.Find(id);
            switch (exercis.Title)
            {
                case "10x Push-ups":
                    string str = "<div class='col-md-12 no-padding'>";
                    str += "<div class='col-md-12 no-padding' style='padding: 0; font-weight: bold;'>This is how to perform one repetition:</div>";
                    str += "<div class='col-md-12 no-padding' style='padding: 0;'>1. Lean forward placing hands on edge of surface slightly wider than shoulder-width apart</div>";
                    str += "<div class='col-sm-12 no-padding' style='padding: 0;'>2. Step legs out straight behind with feet tpgether and toes curled under</div>";
                    str += "<div class='col-sm-12 no-padding' style='padding: 0;'>3. Tense every muscle to form a straight line from head to heels</div>";
                    str += "<div class='col-sm-12 no-padding' style='padding: 0;'>4. Lower chest to touch edge of surface with elbows close to sides</div>";
                    str += "<div class='col-sm-12 no-padding' style='padding: 0;'>5. Straighten arma to push bidy back to start position</div>";
                    str += "</div>";
                    exercis.Muscles = "chest, shoulders, triceps, upper back, abs";
                    exercis.Image = "<img src='/img/index.jpg' height=300 width=300/>";
                    exercis.Directions = str;
                    break;
                case "10x Iron crosses":
                    string str1 = "<div class='col-md-12 no-padding'>";
                    str1 += "<div class='col-md-12 no-padding' style='padding: 0; font-weight: bold;'>This is how to perform one repetition:</div>";
                    str1 += "<div class='col-md-12 no-padding' style='padding: 0;'>1. Lie on back with arma extended to sides at shoulder-level and palms pressing into floor</div>";
                    str1 += "<div class='col-sm-12 no-padding' style='padding: 0;'>2. Lift feet, bending at the knees</div>";
                    str1 += "<div class='col-sm-12 no-padding' style='padding: 0;'>3. Keep knees together and rotate at waist in a controled manner, dropping knees to touch floor</div>";
                    str1 += "<div class='col-sm-12 no-padding' style='padding: 0;'>4. Bring knees back to center directly above hips</div>";
                    str1 += "</div>";
                    exercis.Muscles = "obliques, lats, core";
                    exercis.Image = "<img src='/img/index.jpg' height=300 width=300/>";
                    exercis.Directions = str1;
                    break;
                case "10x Mountain climbers":
                    string str2 = "<div class='col-md-12 no-padding'>";
                    str2 += "<div class='col-md-12 no-padding' style='padding: 0; font-weight: bold;'>This is how to perform one repetition:</div>";
                    str2 += "<div class='col-md-12 no-padding' style='padding: 0;'>1. Place hands on floor directly under shoulders, hips lifted, and extend legs with feet together and toes curled under</div>";
                    str2 += "<div class='col-sm-12 no-padding' style='padding: 0;'>2. Tense every muscle to keep body in a straight line from head through to heels</div>";
                    str2 += "<div class='col-sm-12 no-padding' style='padding: 0;'>3. Keep left foot straight out behind as you pull right knee in towards left elbow</div>";
                    str2 += "<div class='col-sm-12 no-padding' style='padding: 0;'>4. Place right foot back down behind and pull left knee in towards right elbow</div>";
                    str2 += "</div>";
                    exercis.Muscles = "core, glutes, quadriceps, hamstrings";
                    exercis.Image = "<img src='/img/index.jpg' height=300 width=300/>";
                    exercis.Directions = str2;
                    break;
                case "Russian twists":
                    string str3 = "<div class='col-md-12 no-padding'>";
                    str3 += "<div class='col-md-12 no-padding' style='padding: 0; font-weight: bold;'>This is how to perform one repetition:</div>";
                    str3 += "<div class='col-md-12 no-padding' style='padding: 0;'>1. Sit on floor with knees bent at 45 degrees and heels several inches above floor</div>";
                    str3 += "<div class='col-sm-12 no-padding' style='padding: 0;'>2. Lean torso back with 45 degrees bend in hips and balance on your sit bones</div>";
                    str3 += "<div class='col-sm-12 no-padding' style='padding: 0;'>3. Rotate torso to tap both hands on floor by left hip</div>";
                    str3 += "<div class='col-sm-12 no-padding' style='padding: 0;'>4. Rotate torso in opposite direction to tap both hands on floor by right hip</div>";
                    str3 += "</div>";
                    exercis.Muscles = "abs, quadriceps";
                    exercis.Image = "<img src='/img/index.jpg' height=300 width=300/>";
                    exercis.Directions = str3;
                    break;
                case "00:20 Fast feet":
                    string str4 = "<div class='col-md-12 no-padding'>";
                    str4 += "<div class='col-md-12 no-padding' style='padding: 0; font-weight: bold;'>This is how to perform one repetition:</div>";
                    str4 += "<div class='col-md-12 no-padding' style='padding: 0;'>1. Stand with feet hip-width apart, arms at sides</div>";
                    str4 += "<div class='col-sm-12 no-padding' style='padding: 0;'>2. Push on the balls of feet</div>";
                    str4 += "<div class='col-sm-12 no-padding' style='padding: 0;'>3. Run on the spot, lightly and quickly</div>";
                    str4 += "</div>";
                    exercis.Muscles = "calves, quadriceps, hamstrings, glutes";
                    exercis.Image = "<img src='/img/index.jpg' height=300 width=300/>";
                    exercis.Directions = str4;
                    break;
                case "00:20 Boat":
                    string str5 = "<div class='col-md-12 no-padding'>";
                    str5 += "<div class='col-md-12 no-padding' style='padding: 0; font-weight: bold;'>This is how to perform one repetition:</div>";
                    str5 += "<div class='col-md-12 no-padding' style='padding: 0;'>1. Lie faceup with lower back pressed into floor</div>";
                    str5 += "<div class='col-sm-12 no-padding' style='padding: 0;'>2. Keep legs straight feet together and raise several inches off floor</div>";
                    str5 += "<div class='col-sm-12 no-padding' style='padding: 0;'>3. Lift shoulders 4 inches off floor with arms by your ears</div>";
                    str5 += "<div class='col-sm-12 no-padding' style='padding: 0;'>4. Contract core and hold position for indicated time</div>";
                    str5 += "</div>";
                    exercis.Muscles = "abs, lower back, shoulders, quadriceps";
                    exercis.Image = "<img src='/img/index.jpg' height=300 width=300/>";
                    exercis.Directions = str5;
                    break;
                case "8x Push-ups":
                    string str6 = "<div class='col-md-12 no-padding'>";
                    str6+= "<div class='col-md-12 no-padding' style='padding: 0; font-weight: bold;'>This is how to perform one repetition:</div>";
                    str6 += "<div class='col-md-12 no-padding' style='padding: 0;'>1. Begin with hands directly under shoulders and feet slightly wider than hip-width apart on floor</div>";
                    str6 += "<div class='col-sm-12 no-padding' style='padding: 0;'>2. Step legs behind with feet together, toes curled under, and lift hips with chest in front of hands</div>";
                    str6 += "<div class='col-sm-12 no-padding' style='padding: 0;'>3. Tense every muscle to form a straight line from head to heels throughout</div>";
                    str6 += "<div class='col-sm-12 no-padding' style='padding: 0;'>4. Lower chest to touch floor with elbows tight to body</div>";
                    str6 += "<div class='col-sm-12 no-padding' style='padding: 0;'>5. Extend arms to push body back to start position</div>";
                    str6 += "</div>";
                    exercis.Muscles = "chest, shoulders, triceps, upper back, abs";
                    exercis.Image = "<img src='/img/index.jpg' height=300 width=300/>";
                    exercis.Directions = str6;
                    break;
                case "8x Squats":
                    string str7 = "<div class='col-md-12 no-padding'>";
                    str7 += "<div class='col-md-12 no-padding' style='padding: 0; font-weight: bold;'>This is how to perform one repetition:</div>";
                    str7 += "<div class='col-md-12 no-padding' style='padding: 0;'>1. Stand straight with shoulders above hips, feet hip-width apart, toes slightly turned out</div>";
                    str7 += "<div class='col-sm-12 no-padding' style='padding: 0;'>2. Drop hips back and down; keep knees behind toes at all times and hip and ankle in line so knees do not collapse inward</div>";
                    str7 += "<div class='col-sm-12 no-padding' style='padding: 0;'>3. Drop hips as low as possible</div>";
                    str7 += "<div class='col-sm-12 no-padding' style='padding: 0;'>4. Drive weight into heels to stand back up to start position</div>";
                    str7 += "</div>";
                    exercis.Muscles = "glutes, hamstrings, quadriceps, lower back, abs";
                    exercis.Image = "<img src='/img/index.jpg' height=300 width=300/>";
                    exercis.Directions = str7;
                    break;
                default:
                    break;
            }
            if (exercis == null)
            {
                return HttpNotFound();
            }
            return View(exercis);
        }

        // GET: ExercisesPacks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ExercisesPacks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Calories,Time")] ExercisesPack exercisesPack)
        {
            if (ModelState.IsValid)
            {
                db.ExercisesPacks.Add(exercisesPack);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(exercisesPack);
        }

        // GET: ExercisesPacks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExercisesPack exercisesPack = db.ExercisesPacks.Find(id);
            if (exercisesPack == null)
            {
                return HttpNotFound();
            }
            return View(exercisesPack);
        }

        // POST: ExercisesPacks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Calories,Time")] ExercisesPack exercisesPack)
        {
            if (ModelState.IsValid)
            {
                db.Entry(exercisesPack).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(exercisesPack);
        }

        // GET: ExercisesPacks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExercisesPack exercisesPack = db.ExercisesPacks.Find(id);
            if (exercisesPack == null)
            {
                return HttpNotFound();
            }
            return View(exercisesPack);
        }

        // POST: ExercisesPacks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ExercisesPack exercisesPack = db.ExercisesPacks.Find(id);
            db.ExercisesPacks.Remove(exercisesPack);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
