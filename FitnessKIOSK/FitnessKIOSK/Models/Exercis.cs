namespace FitnessKIOSK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Exercises")]
    public partial class Exercis
    {
        //public IDictionary<int, string> Muscles { get; set; }
        //public IDictionary<int, string> Directions { get; set; }
        ////public IDictionary<int, string> DirectionsData { get; set; }
        //public IDictionary<int, string> Tips { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Exercis()
        {

            Likes = new HashSet<Like>();

            //Muscles = new Dictionary<int, string>();
            //Directions = new Dictionary<int, string>();
            ////DirectionsData = new Dictionary<int, string>();
            //Tips = new Dictionary<int, string>();

            //Muscles.Add(1, "chest, shoulders, triceps, upper back, abs");
            //Muscles.Add(2, "obliques, lats, core");
            //Muscles.Add(3, "core, glutes, quadriceps, hamstrings");
            //Muscles.Add(4, "quadriceps, abs");
            //Muscles.Add(5, "calves, quadriceps, hamstrings, glutes");
            //Muscles.Add(6, "abs, lower back, shoulders, quadriceps");
            //Muscles.Add(7, "upper back");
            //Muscles.Add(9, "glutes, hamstrings, quadriceps");
            //Muscles.Add(10, "chest, shoulders, triceps, upper back, abs");
            //Muscles.Add(11, "shoulders, triceps, upper back");
            //Muscles.Add(12, "glutes, hamstrings, quadriceps, lower back, abs");

            //Directions.Add(1, "");
            //Directions.Add(2, "");
            //Directions.Add(3, "");
            //Directions.Add(4, "");
            //Directions.Add(5, "");
            //Directions.Add(6, "");
            //Directions.Add(7, "");
            //Directions.Add(9, "");
            //Directions.Add(10, "Begin with hands");
            //Directions.Add(11, "");
            //Directions.Add(12, "");
        }

        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        public int ExercisesPackId { get; set; }

        [NotMapped]
        public string Muscles { get; set; }

        [NotMapped]
        public string Directions { get; set; }

        [NotMapped]
        public string Image { get; set; }

        public virtual ExercisesPack ExercisesPack { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Like> Likes { get; set; }
    }
}
