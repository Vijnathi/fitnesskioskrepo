namespace FitnessKIOSK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BodyMassIndex")]
    public partial class BodyMassIndex
    {
        public int Id { get; set; }

        public double Height { get; set; }

        public double Weight { get; set; }

        [Required]
        [StringLength(10)]
        public string Gender { get; set; }

        public int Age { get; set; }

        [Column(TypeName = "date")]
        public DateTime CalculatedOn { get; set; }

        public double BMIValue { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}
