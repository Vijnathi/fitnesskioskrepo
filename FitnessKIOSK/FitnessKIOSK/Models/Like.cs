namespace FitnessKIOSK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Like
    {
        public int Id { get; set; }

        public bool isLike { get; set; }

        public int UserId { get; set; }

        public int ExercisesId { get; set; }

        public virtual Exercis Exercis { get; set; }

        public virtual User User { get; set; }
    }
}
