namespace FitnessKIOSK.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FitnessKIOSK_Model : DbContext
    {
        public FitnessKIOSK_Model()
            : base("name=FitnessKIOSK_Model")
        {
        }

        public virtual DbSet<BodyMassIndex> BodyMassIndexes { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<Exercis> Exercises { get; set; }
        public virtual DbSet<ExercisesPack> ExercisesPacks { get; set; }
        public virtual DbSet<Like> Likes { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Exercis>()
                .HasMany(e => e.Likes)
                .WithRequired(e => e.Exercis)
                .HasForeignKey(e => e.ExercisesId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ExercisesPack>()
                .HasMany(e => e.Exercises)
                .WithRequired(e => e.ExercisesPack)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BodyMassIndexes)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Events)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Likes)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);
        }
    }
}
