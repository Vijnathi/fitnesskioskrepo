﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FitnessKIOSK.Startup))]
namespace FitnessKIOSK
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
