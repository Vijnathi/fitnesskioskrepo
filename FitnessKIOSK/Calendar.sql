﻿CREATE TABLE [dbo].[Events] (
[Id] int IDENTITY(1,1) NOT NULL,
[Title] nvarchar(max) NOT NULL,
[Start] datetime NOT NULL,
PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO